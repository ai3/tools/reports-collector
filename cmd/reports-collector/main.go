package main

import (
	"context"
	"flag"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	rc "git.autistici.org/ai3/tools/reports-collector"
	"github.com/chrj/smtpd"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"golang.org/x/sync/errgroup"
)

var (
	addr     = flag.String("addr", fromEnv("ADDR", ":4890"), "address to listen on (HTTP)")
	smtpAddr = flag.String("smtp-addr", fromEnv("SMTP_ADDR", ""), "address to listen on (SMTP), disable incoming SMTP if empty")

	gracefulShutdownTimeout = 5 * time.Second
	maxMessageSize          = 20 * 1024 * 1024
)

func fromEnv(name, deflt string) string {
	if s := os.Getenv(name); s != "" {
		return s
	}
	return deflt
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	collector := rc.NewCollector(
		new(rc.LogSink),
		new(rc.ReportHandler),
		new(rc.TLSRPTHandler),
		new(rc.LegacyCSPHandler),
		new(rc.DMARCHandler),
	)

	// Crete an errgroup.Group with a controlling Context that
	// will be canceled if any of our protocol servers fail to
	// start.
	outerCtx, cancel := context.WithCancel(context.Background())
	g, ctx := errgroup.WithContext(outerCtx)

	// Create the http.Server.
	g.Go(func() error {
		mux := http.NewServeMux()
		mux.Handle("/ingest/v1", collector)
		mux.Handle("/metrics", promhttp.Handler())
		server := &http.Server{
			Addr:         *addr,
			Handler:      mux,
			ReadTimeout:  10 * time.Second,
			IdleTimeout:  30 * time.Second,
			WriteTimeout: 10 * time.Second,
		}

		go func() {
			<-ctx.Done()
			if ctx.Err() != context.Canceled {
				return
			}
			// Gracefully terminate, then shut
			// down remaining clients.
			sctx, scancel := context.WithTimeout(
				context.Background(),
				gracefulShutdownTimeout)
			defer scancel()
			if err := server.Shutdown(sctx); err == context.Canceled {
				if err := server.Close(); err != nil {
					log.Printf("error terminating server: %v", err)
				}
			}
		}()

		log.Printf("starting HTTP server on %s", *addr)
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			return err
		}
		return nil
	})

	// Create the SMTP server.
	if *smtpAddr != "" {
		g.Go(func() error {
			hostname, _ := os.Hostname()
			server := &smtpd.Server{
				Hostname:       hostname,
				Handler:        collector.ServeSMTP,
				ReadTimeout:    60 * time.Second,
				WriteTimeout:   60 * time.Second,
				DataTimeout:    60 * time.Second,
				MaxConnections: 100,
				MaxMessageSize: maxMessageSize,
				MaxRecipients:  1,
			}

			// Create our own listener so we can shut down cleanly.
			log.Printf("starting SMTP server on %s", *smtpAddr)
			l, err := net.Listen("tcp", *smtpAddr)
			if err != nil {
				log.Fatal(err)
			}
			go func() {
				<-ctx.Done()
				l.Close()
			}()

			return server.Serve(l)
		})
	}

	// Cancel the outer context if we receive a termination
	// signal. This will cause the servers to be stopped.
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("signal received, terminating")
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	err := g.Wait()
	if err != nil && err != context.Canceled {
		log.Printf("error: %v", err)
	}
}
