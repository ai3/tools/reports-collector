FROM golang:latest AS build
ADD . /src
RUN cd /src && go test -v . && go build -tags netgo -o reports-collector ./cmd/reports-collector && strip reports-collector

FROM scratch
COPY --from=build /src/reports-collector /reports-collector
CMD ["/reports-collector"]
