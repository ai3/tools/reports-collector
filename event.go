package reportscollector

type Event map[string]interface{}

func (e Event) Set(key string, value interface{}) {
	e[key] = value
}

func (e Event) GetString(key string) string {
	return e[key].(string)
}
