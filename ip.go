package reportscollector

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"sync"

	geoip2 "github.com/oschwald/geoip2-golang"
)

var (
	geoipDbPath = "/var/lib/GeoIP/GeoLite2-ASN.mmdb"
	geodb       *geoip2.Reader
	geodbInit   sync.Once
)

func initGeoIP() {
	if s := os.Getenv("GEOIP_DB_PATH"); s != "" {
		geoipDbPath = s
	}

	var err error
	geodb, err = geoip2.Open(geoipDbPath)
	if err != nil {
		log.Printf("warning: could not open GeoIP db: %v", err)
		return
	}
}

func lookupASN(addr string) (string, bool) {
	geodbInit.Do(initGeoIP)

	if geodb != nil {
		ip := net.ParseIP(addr)
		if ip == nil {
			return "", false
		}
		asn, err := geodb.ASN(ip)
		if err == nil {
			return fmt.Sprintf("AS%d", asn.AutonomousSystemNumber), true
		}
	}
	return "", false
}

func getRemoteIP(req *http.Request) string {
	if addr := req.Header.Get("X-Forwarded-For"); addr != "" {
		return addr
	}
	if addr, _, err := net.SplitHostPort(req.RemoteAddr); err == nil {
		return addr
	}
	return ""
}
