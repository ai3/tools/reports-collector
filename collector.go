package reportscollector

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"mime"
	"net/http"

	"github.com/chrj/smtpd"
	"github.com/jhillyerd/enmime"
	"github.com/prometheus/client_golang/prometheus"
)

var ErrNoMatch = errors.New("no match")

type Handler interface {
	Name() string
	Parse(string, *http.Request) ([]Event, error)
	ParseMIME(*enmime.Part) ([]Event, error)
}

type Sink interface {
	Send(Event)
}

type Collector struct {
	handlers []Handler
	sink     Sink
}

func NewCollector(sink Sink, handlers ...Handler) *Collector {
	return &Collector{
		handlers: handlers,
		sink:     sink,
	}
}

func (c *Collector) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodPost:
	case http.MethodOptions:
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST")
		w.Header().Set("Access-Control-Allow-Headers", "content-type")
		w.Header().Set("Access-Control-Max-Age", "86400")
		w.WriteHeader(http.StatusNoContent)
		return
	default:
		http.Error(w, "Bad method", http.StatusMethodNotAllowed)
		return
	}

	ct, _, err := mime.ParseMediaType(req.Header.Get("Content-Type"))
	if err != nil {
		http.Error(w, fmt.Sprintf("Bad Content-Type: %v", err.Error()), http.StatusBadRequest)
		log.Printf("error parsing content-type: %v", err)
		return
	}

	// Find a handler that can successfully parse the request, and
	// get a list of Events.
	for _, h := range c.handlers {
		events, err := h.Parse(ct, req)
		switch err {
		case nil:
			// Send the parsed events to the Sink.
			for _, e := range events {
				c.sink.Send(e)
				reportsByType.WithLabelValues(
					e.GetString("type"), e.GetString("domain")).Inc()
			}

			log.Printf("http: %s: received %d events from %s", h.Name(), len(events), getRemoteIP(req))
			w.WriteHeader(http.StatusOK)
			return
		case ErrNoMatch:
			continue
		default:
			log.Printf("error parsing report (%s): %v", ct, err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	log.Printf("no matching handlers for \"%s\"", ct)
	http.Error(w, "No matching handlers", http.StatusBadRequest)
}

func (c *Collector) ServeSMTP(peer smtpd.Peer, env smtpd.Envelope) error {
	msgParts, err := enmime.ReadParts(bytes.NewReader(env.Data))
	if err != nil {
		log.Printf("smtp: error parsing input: %v", err)
		return smtpd.Error{550, "Error parsing input"}
	}

	// Find a handler that can successfully parse the request, and
	// get a list of Events.
	for _, h := range c.handlers {
		events, err := h.ParseMIME(msgParts)
		switch err {
		case nil:
			// Send the parsed events to the Sink.
			for _, e := range events {
				c.sink.Send(e)
				reportsByType.WithLabelValues(
					e.GetString("type"), e.GetString("domain")).Inc()
			}
			log.Printf("smtp: %s: received %d events from %s", h.Name(), len(events), env.Sender)
			return nil
		case ErrNoMatch:
			continue
		default:
			log.Printf("smtp: error handling report: %v", err)
			// Discard the message.
			return nil
		}
	}

	log.Printf("smtp: no matching handlers")
	// Discard the message.
	return nil
}

var (
	reportsByType = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "reports_total",
			Help: "Number of reports by type.",
		},
		[]string{"type", "domain"},
	)
)

func init() {
	prometheus.MustRegister(reportsByType)
}
