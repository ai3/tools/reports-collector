package reportscollector

import (
	"encoding/json"
	"log"
)

type LogSink struct {
}

func (*LogSink) Send(e Event) {
	data, err := json.Marshal(e)
	if err != nil {
		return
	}
	log.Printf("@cee:%s", data)
}
